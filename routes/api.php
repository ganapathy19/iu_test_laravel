<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// RESOURCE //
Route::resources([
    'user_manage' => 'UserController',
]);

// METHODS //
Route::get('/user_remove', 'UserController@user_remove');
Route::post('/user_register', 'UserController@register');
Route::post('/user_login', 'UserController@login');
Route::get('/gmail_list', 'UserController@gmail_list');
Route::post('/gmail_remove', 'UserController@gmail_remove');
