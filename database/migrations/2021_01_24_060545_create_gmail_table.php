<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gmail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->nullable();
            $table->string('content')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('time')->nullable();
            $table->string('check')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gmail');
    }
}
