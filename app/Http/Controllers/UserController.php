<?php

namespace App\Http\Controllers;

use App\Models\user;
use App\Models\Register;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items_per_page = $request->input('items_per_page');
        $current_page_no = $request->input('current_page_no');
        $search = $request->input('search');
        $current_date = date("Y-m-d"); 

        if (isset($items_per_page, $current_page_no)) {
            $current_page_starting_no = ($items_per_page * ($current_page_no - 1));

            $query = user::where('status', 1);

            if (isset($search)) {
                $query = $query->where(function ($search_query) use ($search) {
                    $search_query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                        ->orWhere('d_o_j', 'LIKE', '%' . $search . '%')
                        ->orWhere('d_o_r', 'LIKE', '%' . $search . '%');
                });
            }
            $total_record_count = $query->count();
             $data = $query->skip($current_page_starting_no)
                ->take($items_per_page)
                ->get();

            if(count($data) > 0) {
                // Start Calculate Experience
                foreach($data as $value) {
                    if(isset($value->d_o_j) && isset($value->d_o_r)) {
                        $diff = abs(strtotime($value->d_o_j) - strtotime($value->d_o_r));
                    } else { 
                        $diff = abs(strtotime($value->d_o_j) - strtotime($current_date));
                    }
                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                    
                        if($years == 0 && $months == 0) {
                            $value->experience = $days.' day';
                        } else if ($years == 0) {
                            $value->experience = $months.' month';
                        } else if ($months == 0) {
                            $value->experience = $years.' year';
                        } else {
                            $value->experience = $years.' year'.$months.' month';
                        }
                }
                // End Calculate Experience
            }

            $pagination_array = array('status' => 'success', 'total_record_count' => $total_record_count, 'data' => $data);

        } else {
            $pagination_array = array('total_record_count' => 0, 'data' => 'Please give a pagination parameters');
        }

        return $pagination_array;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request->all();

        $id = $request->input('id');

        $rules = array(
            'name' => 'required',
            'email' => 'required',
            'd_o_j' => 'required',
            'profile' => 'required',
            // 'profile' => ['required', 'mimes:jpg,jpeg,png', 'max:80'],
        );

        $Validation = \Validator::make($request->all(), $rules);
        if ($Validation->fails()) {
            $data = array('status' => 'error', 'message' => 'Validation Failed', 'errors' => $Validation->errors());
        } else {

            
            if (isset($id) && $id != 'null') {
                $data = user::find($id);
            } else {
                $data = new user();

            }
            
                $data->name = $request->input('name');
                $data->email = $request->input('email');
                $data->d_o_j = $request->input('d_o_j');
                $data->d_o_r = $request->input('d_o_r');
                    if ($request->input('still_wrk') == 'null') {
                        $data->still_wrk = false;
                    } else {
                        $data->still_wrk = $request->input('still_wrk');
                    }
                // $data->still_wrk = 2;
                // $data->save();

                    $profile_str = $request->input('profile_str');
                    $profile_picture = $request->file('profile');

                    // return array($profile_str, $profile_picture);

                    if (isset($profile_str)) {
                        $data->profile = $profile_str;
                        $data->save();
                    }

                    if (isset($profile_picture)) {
                        $old_file_full_path = $profile_picture;
                        if (isset($old_file_full_path)) {
                            user::img_delete($old_file_full_path);
                        }
                        $full_file_name_with_path = user::image_upload($profile_picture, 'users/');
        
                        // $data = user::find($data->id);
                        $data->profile = $full_file_name_with_path;
                        $data->save();
                    }

        }

        if (isset($id) && $id != 'null') {
            $data = array('status' => 'success', 'message' => 'User Updated Successfully');
        } else {
            $data = array('status' => 'success', 'message' => 'User Added Successfully');
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $current_date = date("Y-m-d"); 
        $query = user::where('id', $id)->first();

            // Start Calculate Experience
            if(isset($query->d_o_j) && isset($query->d_o_r)) {
                $diff = abs(strtotime($query->d_o_j) - strtotime($query->d_o_r));
            } else { 
                $diff = abs(strtotime($query->d_o_j) - strtotime($current_date));
            }
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            
                if($years == 0 && $months == 0) {
                    $query->experience = $days.' day';
                } else if ($years == 0) {
                    $query->experience = $months.' month';
                } else if ($months == 0) {
                    $query->experience = $years.' year';
                } else {
                    $query->experience = $years.' year'.$months.' month';
                }
            // End Calculate Experience

        $data = array('status' => 'success', 'message' => 'User Info Fetched Successfully', 'data' => $query);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = user::where('id', $id)->first();
        $data = array('status' => 'success', 'message' => 'User Info Fetched Successfully', 'data' => $query);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function user_remove(Request $request)
    {
        $user_id = $request->input('user_id');

        $data = user::find($user_id);
        $data->status = 0;
        $data->save();

        return $data = array('status' => 'success', 'message' => 'User Removed Successfully');
    }

    public function check_api() {

        $date1 = "2007-03-24";
        $date2 = "2009-06-26";

        $diff = abs(strtotime($date2) - strtotime($date1));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        // printf("%d years, %d months, %d days\n", $years, $months, $days);

        return $years.'year'.$months.'month'.$days.'day';

    }

    public function register(Request $request)
    {

        // return $request->all();

        $id = $request->input('id');

        $rules = array(
            'first_name' => 'required',
            'email' => 'required',
            'password' => 'required',
        );

        $Validation = \Validator::make($request->all(), $rules);
        if ($Validation->fails()) {
            $data = array('status' => 'error', 'message' => 'Validation Failed', 'errors' => $Validation->errors());
        } else {

            
            if (isset($id) && $id != 'null') {
                $data = Register::find($id);
            } else {
                $data = new Register();

            }
            
                $data->first_name = $request->input('first_name');
                $data->last_name = $request->input('last_name');
                $data->email = $request->input('email');
                $data->mobile_no = $request->input('mobile_no');
                // $data->gender = $request->input('gender');
                $data->address = $request->input('address');
                $data->password = $request->input('password');
                $data->save();

        }

        if (isset($id) && $id != 'null') {
            $data = array('status' => 'success', 'message' => 'Profile Updated Successfully');
        } else {
            $data = array('status' => 'success', 'message' => 'Register Successfully');
        }

        return $data;
    }

    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        $data = user::where('email', $email)->where('password', $password)->first();

        if(isset($data) && $data != null) {
            return $data = array('status' => 'success', 'message' => 'Login Successfully', 'data' => $data);
        } else {
            return $data = array('status' => 'error', 'message' => 'Invalid Credentials');
        }

    }

    public function gmail_list(Request $request)
    {
        $items_per_page = $request->input('items_per_page');
        $current_page_no = $request->input('current_page_no');
        $search = $request->input('search');

        if (isset($items_per_page, $current_page_no)) {
            $current_page_starting_no = ($items_per_page * ($current_page_no - 1));
            $ending_no = $items_per_page * $current_page_no;
            $star_no = ($items_per_page * ($current_page_no - 1));

            if($star_no == 0) {
                $starting_no = 1;
            } else {
                $starting_no = $star_no;
            }

            $query = Register::where('status', 1);

            if (isset($search)) {
                $query = $query->where(function ($search_query) use ($search) {
                    $search_query->where('subject', 'LIKE', '%' . $search . '%')
                        ->orWhere('content', 'LIKE', '%' . $search . '%')
                        ->orWhere('from', 'LIKE', '%' . $search . '%')
                        ->orWhere('time', 'imeLIKE', '%' . $search . '%')
                        ->orWhere('to', 'LIKE', '%' . $search . '%');
                });
            }
            $total_record_count = $query->count();
             $data = $query->skip($current_page_starting_no)
                ->take($items_per_page)
                ->get();

            $total_page_count = $total_record_count / $items_per_page;

            $pagination_array = array('status' => 'success', 'total_record_count' => $total_record_count, 
                'data' => $data, 'starting_no' => $starting_no, 'ending_no' => $ending_no, 
                'current_page_no' => $current_page_no, 'total_page_count' => $total_page_count );

        } else {
            $pagination_array = array('total_record_count' => 0, 'data' => 'Please give a pagination parameters');
        }

        return $pagination_array;
    }

    public function gmail_remove(Request $request)
    {
        $id = $request->input('id');
        $ids_array = $request->input('ids_array');

        if(isset($id)) {
            $data = Register::find($id);
            $data->status = 2;
            $data->save();
        } else {

            $data = Register::get();

            foreach($data as $value) {
                if(isset($ids_array) && $ids_array != []) {
                    foreach($ids_array as $array) {
                        if($array == $value->id) {
                            $remove = Register::find($value->id);
                            $remove->status = 2;
                            $remove->save();
                        }
                    }
                } else {
                    $remove = Register::find($value->id);
                    $remove->status = 2;
                    $remove->save();
                }
            }

        }

        return $data = array('status' => 'success', 'message' => 'Removed Successfully');
    }

}
