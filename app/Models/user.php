<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    // protected $table = 'users';
    protected $table = 'register';

    public static function image_upload($file, $folder_name)
    {
        $destinationPath = 'profile/' . date("Y") . '/' . $folder_name . '/';
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        $number = mt_rand(1000, 9999);
        $ext = $file->guessClientExtension();
        $filename = $number . "." . $ext;
        $file->move($destinationPath, $filename);
        return $file_name_with_full_path = $destinationPath . $filename;
    }

    public static function img_delete($fullpath)
    {
        if (file_exists(public_path($fullpath))) {
            $fullpath = str_replace("", "/", $fullpath);
            unlink($fullpath); //works in local
            $data = ['status' => 'success', 'message' => 'image deleted successfully'];
            return $data;
        }
    }
    
}
